# trivia-test

* [x] install VueRouter - npm install vue-router
* [x] install bootstrap-vue
* [x] Create Views
* [x] Fetch questions from API
* [ ] rout components
* [ ] install VueRouter - npm install vue-router
* [ ] install VueRouter - npm install vue-router



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
