import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Components
import StartScreen from './views/StartScreen'
import QuestionBox from './components/QuestionBox'

Vue.use(BootstrapVue)
Vue.use( VueRouter);

const routes = [
  {
    path: '/start',
    name: "StartScreen",
    component: StartScreen,
  },
  {
    path: '/quiz',
    name: "QuestionBox",
    component: QuestionBox,
  },
  {
    path: '/',
    redirect: '/start'
  }
];

const router = new VueRouter({ routes });

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
