const url = "https://opentdb.com/api.php?amount=10&category=31&difficulty=easy&type=multiple";

export const getResults = () => {
  return fetch( url )
  .then(response => response.json())
  .then(response => {
    if(response.success === false){
      throw Error(response.error)
    }
    return response;
  })
  .then(response => response.results)
}
